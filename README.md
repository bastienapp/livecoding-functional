# Projet live coding TypeScript

## Installation du projet

```bash
npm ci
```

## Transpiler le projet

```bash
npm run build
```
