/**
 * Convertir des degrés Fahrenheit en degrés Celsius
 * @param fahrenheit les degrés en Fahrenheit
 * @returns la valeur convertie en degrés Celcius
 */
function convertFahrenheitToCelsius(fahrenheit: number): number {
  if (fahrenheit > 88888) {
    throw new Error('Valeur d\'entrée incorrecte');
  } else {
    return (fahrenheit - 32) * 5 / 9;
  }
}
// "celcius", "kelvin" et "fahrenheit"
// convertir des degrés, qui sont en entrée des celcius, ou Fahrenheit, ou Kelvin, vers un autre type de degré
// entrée : temperature (number), unitIn (string), unitOut (string) : ex: 14, "kelvin", "celcius"
// sortie : number
type TemperatureUnit = "celcius" | "kelvin" | "fahrenheit"

function convertTemperature(temperature: number, unitIn: TemperatureUnit, unitOut: TemperatureUnit): number {

  switch (unitIn) {
    case "celcius":
      if (unitOut === "kelvin") {
        return temperature + 273.15
      } else if (unitOut === "fahrenheit") {
        return (temperature * 9 / 5) + 32
      } else {
        throw new Error('Invalid unitOut ' + unitOut)
      }
      break;
    case "kelvin":
      if (unitOut === "celcius") {
        return temperature * 273.15
      } else if (unitOut === "fahrenheit") {
        return (temperature - 273.15) * 9 / 5 + 32
      } else {
        throw new Error('Invalid unitOut ' + unitOut)
      }
      break;
    case "fahrenheit":
      if (unitOut === "celcius") {
        return (temperature - 32) * 5 / 9;
      } else if (unitOut === "kelvin") {
        return (temperature - 32) * 5 / 9 + 273.15
      } else {
        throw new Error('Invalid unitOut ' + unitOut)
      }
      break;
    default:
      throw new Error('Invalid unitIn ' + unitIn);
  }
}
/*
convertTemperature(12, 'celcius', 'fahrenheit')

const prompt = require('prompt-sync')();
const temperature = prompt('Température:');
const temperatureNumber = parseFloat(temperature);
if (isNaN(temperatureNumber)) {
  console.log("Merci de donner un nombre")
} else {
  const unitIn = prompt('Unité de départ (celcius, kelvin et fahrenheit):');
  const unitOut = prompt('Unité de d\'arrivé (celcius, kelvin et fahrenheit):');

  const result = convertTemperature(temperatureNumber, unitIn, unitOut)
  console.log(result)
}*/
/*
const testPromise = new Promise((resolve, reject) => {

  setTimeout(() => {
    const random = Math.random();
    if (random > 0.5) {
      resolve(random)
    } else {
      reject("parce que !")
    }
  }, 1000)
})*/
/*
testPromise
  .then((value) => console.log("La valeur est: " + value))
  .catch((error) => console.error(error))
*/
import axios from 'axios';

type Post = {
  userId: number,
  id: number,
  title: string,
  body: string
}

function getPostList() {

  const promiseGetPostList = new Promise<Post[]>((resolve, reject) => {

    axios.get('https://jsonplaceholder.typicode.com/posts')
      .then((response) => {
        resolve(response.data)
      })
      .catch((error) => {
        reject(error)
      }) // equivalent de try/catch
  })
  return promiseGetPostList;
}

const arrayShowPostList = async () => {

}

async function showAllPost() {
  const postListPromise = getPostList();
  postListPromise
    .then((postList) => {
      for (const post of postList) {
        //console.log(post.title)
      }
    })
    .catch((error) => console.error('cannot get post list'))

  // syntaxic sugar : async / await
  try {
    const newList = await getPostList();
    for (const post of newList) {
      console.log(post.title)
    }
  } catch (error: any) {
    console.error(error.message);
  }
}

showAllPost();